<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

Route::get('login', function () {
    return view('login');
});

Route::get('logout', function () {
    Session::forget('user');
    return view('login');
});

Route::view('/register', 'register');

Route::post('/login', [UserController::class,'login']);
Route::post('/register', [UserController::class,'register']);
Route::get('/', [ProductController::class,'index']);
// Route::get('/', [ProductController::class,'cartItem']);
Route::get('/search', [ProductController::class,'search']);
Route::get('/detail/{id}', [ProductController::class,'detail']);
Route::post('/detail/{id}', [ProductController::class,'addToCart']);
Route::get('/cartList', [ProductController::class,'cartList']);
Route::get('/removeCart/{id}', [ProductController::class,'removeCart']);
Route::get('/order-now', [ProductController::class,'orderNow']);
Route::post('/orderplace', [ProductController::class,'orderPlace']);
Route::get('/myorders', [ProductController::class,'myOrders']);

