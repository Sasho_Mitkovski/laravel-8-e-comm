<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{

    public function run()
    {
        // So = php artisan db:seeder --class=PruductSeeder - ke ja napolnime tabelata
        DB::table('products')->insert([
            [
                'name' => 'LG mobile',
                'price'=> '200',
                'category' => 'mobile',
                'description' => 'Smartphone so 4gb Ram i 128 ROM memory',
                'gallery' => 'https://5.imimg.com/data5/IO/QJ/MY-24529291/lg-mobiles-500x500.png'
            ],
            [
                'name' => 'Samsung mobile',
                'price'=> '150',
                'category' => 'mobile',
                'description' => 'Smartphone so 6gb Ram i 64 ROM memory',
                'gallery' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTwTKFS5U-UabYmqRq-vLmz8NUVU_wmKvmF8Q&usqp=CAU'
            ],
            [
                'name' => 'MI mobile',
                'price'=> '400',
                'category' => 'mobile',
                'description' => 'Smartphone so 8gb Ram i 256 ROM memory',
                'gallery' => 'https://static.digit.in/default/702092e70d593cdd2dc391237e83dd2055d12734.jpeg'
            ],
            [
                'name' => 'HUAWEI mobile',
                'price'=> '550',
                'category' => 'mobile',
                'description' => 'Smartphone so 12gb Ram i 128 ROM memory',
                'gallery' => 'https://static.digit.in/default/49bc7de3227c306b345837e8aaa9fa066066e569.jpeg'
            ],
            [
                'name' => 'POCOPHONE mobile',
                'price'=> '720',
                'category' => 'mobile',
                'description' => 'Smartphone so 16gb Ram i 256 ROM memory',
                'gallery' => 'https://www.mi4canada.com/wp-content/uploads/2020/09/Xiaomi-POCO-X3-NFC-4G-blue-500x500.jpg'
            ],
            [
                'name' => 'SONY tv',
                'price'=> '1400',
                'category' => 'tv',
                'description' => 'Smarttv so 3gbRam i 16 ROM memory',
                'gallery' => 'https://www.sony.co.uk/image/2a8264bd4060bb485e1deeeec1c5564a?fmt=png-alpha&resMode=bisharp&wid=384'
            ],
            [
                'name' => 'Panasonic tv',
                'price'=> '750',
                'category' => 'tv',
                'description' => 'Smart tv so 16 ROM memory',
                'gallery' => 'https://media.wired.com/photos/5932be2ea30e27707249acf4/master/w_2560%2Cc_limit/BRAVIA-929.jpeg'
            ],
            [
                'name' => 'Gorenje fridge',
                'price'=> '250',
                'category' => 'fridge',
                'description' => 'A Fridge with much more feature',
                'gallery' => 'http://mobileimages.lowes.com/product/converted/819130/819130026954.jpg'
            ],
            [
                'name' => 'Samsung fridge',
                'price'=> '320',
                'category' => 'fridge',
                'description' => 'A Fridge with much more feature',
                'gallery' => 'https://5.imimg.com/data5/PR/TE/MY-7846959/635l-french-door-fridge-freezer-500x500.png',
            ],
        ]);
    }
}
