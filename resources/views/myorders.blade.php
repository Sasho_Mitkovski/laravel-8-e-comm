@extends('master')
@section('content')
    <div class="custom-product container">

        <div class="col-sm-10">
            <h3>Купени производи</h3>
            <div class="trending-wrapper">

                @foreach ($orders as $item)
                    <div class="row searched-item cart-list-devider">
                        <div class="col-sm-3">
                            <a href="/detail/{{ $item->id }}">
                                <img src="{{ $item->gallery }}" class="trending-image ">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/detail/{{ $item->id }}">
                                <h3><span><small>Name :</small></span> {{ $item->name }}</h3>
                                <p><span><small>Delivery status :</small></span> {{ $item->status }}</p>
                                <p><span><small>Address :</small></span> {{ $item->address }}</p>
                                <p><span><small>Payment Status :</small></span> {{ $item->payment_status }}</p>
                                <p> <span><small>Payment Method :</small></span>{{ $item->payment_method }}</p>
                            </a>
                        </div>


                    </div>
                @endforeach
            </div>
        </div>
        {{-- <div>
            <a href="/" class="btn btn-secondary">Врати се назад</a>
        </div> --}}


    @endsection
