@extends('master')
@section('content')
    <div class="container custom-login">
        <div class="row">
            <div class="col-md-4 offset-4 mt-5">
                <div class="card">
                    <div class="card-header bg-dark text-light">
                        Login
                    </div>
                    <div class="card-body">
                        <form action="/register" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="name">User name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    aria-describedby="nameHelp" placeholder="Enter your username">
                                <small id="emailHelp" class="form-text text-muted">Вашиот username е безбеден</small>
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                    aria-describedby="emailHelp" placeholder="Enter e-mail">
                                <small id="emailHelp" class="form-text text-muted">Вашиот e-mail е безбеден</small>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                    placeholder="Enter password">
                                <small id="emailHelp" class="form-text text-muted">Вашиот password е безбеден</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Register</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
