
<?php
use App\Http\Controllers\ProductController;
// Po default ke e 0
$total = 0;
if(Session::has('user')){
   $total = ProductController::cartItem();//Zemame vrednot od cardItem method vo ProductController
//  Za da zememe vrednosta, metodot treba da e static
}
?>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <a class="navbar-brand text-success" href="/"> E-Comm</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/myorders">Orders</a>
            </li>
            <form class="form-inline">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>

        </ul>
    </div>
    <ul class="navbar-nav mr-auto">
        {{-- Vkupno products vo kosnickata --}}
        <li class="nav-item"><a class="nav-link text-danger" href="/cartList"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-cart-check" viewBox="0 0 16 16">
            <path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
            <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
          </svg>({{ $total }})</a></li>
   {{-- Login i Logout --}}
   {{-- AKo e logiran, ke prikaze dropdown za login, ako ne ke pokaze link do login strana --}}
         @if(Session()->has('user'))
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{Session()->get('user')['name']}}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/logout">Logout</a>
      </li>
      @else
      <li><a class="nav-link" href="/login">Login</a></li>
      <li><a class="nav-link" href="/register">Register</a></li>
      @endif
    </ul>
</nav>
