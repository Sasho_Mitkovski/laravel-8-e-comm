@extends('master')
@section('content')
<div class="custom-product container">
    <div class="col-sm-10">
        <h3>Купи производ</h3><br>
        <table class="table">
            <tbody>
                <tr>
                    <th>Amount</th>
                    <td>$ {{ $total }}</td>
                </tr>
                <tr>
                    <th>Tax</th>
                    <td>$ 0</td>
                </tr>
                <tr>
                    <th>Delivery</th>
                    <td>10</td>
                </tr>
                <tr>
                    <th>Total amount</th>
                    <td>{{ $total + 10 }}</td>
                </tr>
            </tbody>
        </table>
        {{-- Forma --}}
        <div class="col-sm-8">
            <form action="/orderplace" method="POST">
                @csrf
                <div class="form-group">

                  <textarea type="text" name="address" class="form-control" placeholder="Enter your address" ></textarea>
                </div>
                <div class="form-group">
                  <label for="payment">Payment method:</label><br>
                 <input type="radio" value="cash" name="payment" id="payment" ><span> online payment</span><br>
                 <input type="radio" value="cash" name="payment" id="payment" ><span> EMI payment</span><br>
                 <input type="radio" value="cash" name="payment" id="payment" ><span> payment on delivery</span><br>
                </div>
                <button type="submit" class="btn btn-primary">All Order now</button>
              </form>
        </div>
    </div>
    {{-- <div>
            <a href="/" class="btn btn-secondary">Врати се назад</a>
        </div> --}}
    @endsection
