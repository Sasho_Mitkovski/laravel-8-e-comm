@extends('master')
@section('content')
    <div class=" custom-product">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                @foreach ($products as $item)
                    {{-- Item so ID=1 ke ima class "active" --}}
                    <div class="item {{ $item['id'] == 1 ? 'active' : '' }}">
                        <a href="/detail/{{ $item->id }}">
                            <img src="{{ $item->gallery }}" class="slider-img">
                            <div class="carousel-caption slider-text">
                                <h3>{{ $item->name }}</h3>
                                <p>{{ $item['description'] }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="trending-wrapper container">
            <h3>Сите продукти</h3>
            @foreach ($products as $item)
                <a href="/detail/{{ $item->id }}">
                    <div class="trending-item border p-3 m-3 ">
                        <img src="{{ $item->gallery }}" class="trending-image mx-auto d-block">
                        <div class="">
                            <h6 class="text-center">{{ $item->name }}</h3>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
