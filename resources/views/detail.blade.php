@extends('master')
@section('content')
    <div class="container">
        <div>
            <a href="/" class="btn btn-secondary">Врати се назад</a>
        </div>
        <div class="row">
            <div class="col-sm-6 mt-5">
                <img src="{{ $product->gallery }}" class="slider-img">
            </div>
            <div class="col-sm-6 mb-5">
                <div class="d-flex justify-content-between">
                    <form action="add_to_cart" method="POST">
                        @csrf
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <button type="submit" class="btn btn-primary">Додади во кошничка</button>
                    </form>

                    <form action="add_to_cart" method="POST">
                        @csrf
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <button type="submit" class="btn btn-success">Купи</button>
                    </form>

                </div>
                <hr>
                <div class="card">
                    <div class="card-header">
                        <p>Име на продуктот:</p><span>
                            <h2 class="card-title text-center"> {{ $product->name }}</h2>
                        </span>
                    </div>
                    <div class="card-body p-3 list-group">
                        <p>Опис на продуктот:</p>
                        <p> {{ $product->description }}</p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            категорија
                            <h3 class="">{{ $product->category }}</h3>
                        </li>
                    </ul>
                    <div class="card-footer bg-secondary">
                        <ul class="list-group mt-5">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                цена:
                                <h3 class=" text-success font-weight-bold">{{ $product->price }}$</h3>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
