@extends('master')
@section('content')
<div class="custom-product container">

   <div class="col-sm-10">
      <h3>Производи од кошничка</h3>
      <div class="trending-wrapper">

         @foreach ($products as $item)
         <div class="row searched-item cart-list-devider">
            <div class="col-sm-3">
               <a href="/detail/{{ $item->id }}">
                  <img src="{{ $item->gallery }}" class="trending-image ">
               </a>
            </div>
            <div class="col-sm-3">
               <a href="/detail/{{ $item->id }}">
                  <h3>{{ $item->name }}</h3>
                  <p>{{ $item->description }}</p>
               </a>
            </div>
            <div class="col-sm-3">
               <a href="/detail/{{ $item->id }}">
                  <a href="/removeCart/{{ $item->cart_id }}" class="btn btn-warning mt-5 float-right">Remove</a>
               </a>
            </div>

         </div>
         @endforeach
         <label for="" >Купи ги сите производи:</label>
         <div class="">
            <a class="btn btn-danger mt-5 " href="/order-now">All Buy now</a>
         </div>
      </div>
   </div>
   {{-- <div>
            <a href="/" class="btn btn-secondary">Врати се назад</a>
        </div> --}}


   @endsection
