<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    function index()
    {
        $data =  Product::all();
        return view('product', [
            'products' => $data
        ]);
    }
    // Prikaz na eden product
    public function detail($id)
    {
        $product = Product::find($id);
        return view('detail', ['product' => $product]);
    }
    // Dodavanje na product vo Cart
    public function addToCart(Request $req)
    {
        // Ako userot ne e logiran , ke go vrati na login,
        // Ako e logiran , moze da prodolzi so rabota
        if ($req->session()->has('user')) {
            $cart = new Cart();
            $cart->user_id = $req->session()->get('user')['id']; //ID na logiraniot user
            $cart->product_id = $req->product_id; //ID na produktot od hidden poleto od formata
            $cart->created_at = now();
            $cart->updated_at = now();
            $cart->save();
            return redirect('/');
        } else {
            return redirect('/login');
        }
    }
    // Vkupno producti vo Cart
    static function cartItem()
    {
        $userId = Session::get('user')['id'];
        // Vkupno kolku user_id ima vo tabela Cart, koi odgovaraat na logiraniot userId
        return Cart::where('user_id', $userId)->count();
    }
    // Lista na produkti od Cart tabela
    public function cartList()
    {
        $userId = Session::get('user')['id'];
        $products = DB::table('cart')
            ->join('products', 'cart.product_id', '=', 'products.id')
            ->where('cart.user_id', $userId)
            ->select('products.*', 'cart.id as cart_id')
            ->get();
        return view('cart-list', ['products' => $products]);
    }
    public function removeCart($id)
    {
        Cart::destroy($id);
        return redirect('cartList');
    }
    // Zemanje na site produkti od Cart
    public function orderNow()
    {
        $userId = Session::get('user')['id'];
        $total = DB::table('cart')
            ->join('products', 'cart.product_id', '=', 'products.id')
            ->where('cart.user_id', $userId)
            ->sum('products.price');
        return view('order-now', ['total' => $total]);
    }
    // Naracka na site producti od Cart
    public function orderPlace(Request $req)
    {
        $userId = Session::get('user')['id'];
        $allCart = Cart::where('user_id', $userId)->get(); //Kade sto user_id e ednakvo na id na logiraniot korisnik
        foreach ($allCart as $cart) {
            $order = new Order;
            $order->product_id = $cart->product_id;
            $order->user_id = $cart->user_id;
            $order->status = "pending";
            $order->payment_method = $req->payment;
            $order->payment_status = "pending";
            $order->address = $req->address;
            $order->save();
            Cart::where('user_id', $userId)->delete(); // Koga ke gi kupime, da gi izbrishe od Cart tabelata
        }

        return redirect("/");
    }
    //   Istorija na  site kupeni producti
    public function myOrders()
    {
        $userId = Session::get('user')['id'];
       $orders =  DB::table('orders')
            ->join('products', 'orders.product_id', '=', 'products.id')
            ->where('orders.user_id', $userId)
            ->get();
        return view('myorders', ['orders' => $orders]);
    }

}
