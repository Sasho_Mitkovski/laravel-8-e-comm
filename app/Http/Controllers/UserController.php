<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function login(Request $req)
    {
        // Proverka dali emailot e tocen
        $user =  User::where(['email' => $req->email])->first();
        //Ako  vnesenite user i  password ne se isi kako vo baza
        if (!$user || !Hash::check($req->password, $user->password)) {
            //Ako e neuspeshno logiranjeto
            return "Username или Password  се неточни";
        } else {
            $req->session()->put('user', $user); // Ako e uspesno logiranjeto, da se zacuva vo session
            return redirect('/');
        }
    }
    function register(Request $req)
    {
        // return $req->input();
        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->save();
        return redirect('/login');
    }
}
